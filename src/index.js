/* global $, angular, React */

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import logo from './Picture.png';
import './App.css';

const reactFormContainer = document.querySelector('.react-form-container');
class ReactFormLabel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return(
            <label htmlFor={this.props.htmlFor}>{this.props.title}</label>
        )
    }
}
class ReactForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            clientEmail: '',
            location: '',
        }
    }

    handleChange = (e) => {
        let newState = {};

        newState[e.target.name] = e.target.value;

        this.setState(newState);
    };


    handleSubmit = (e, message) => {
        e.preventDefault();

        let formData = {
            formSender: this.state.name,
            formEmail: this.state.clientEmail,
            formLocation: this.state.location,
        };

        if (formData.formSender.length < 1 || formData.formEmail.length < 1 || formData.formLocation.length < 1) {
            return false;
        }
            $.ajax({
            url: '/some/url',
            dataType: 'json',
            type: 'POST',
            data: formData,
            success: function(data) {
                if (window.confirm('Thank you for using upgrdphotos!')) {
                    document.querySelector('.form-input').val('');
                }
            },
            error: function(xhr, status, err) {
                console.error(status, err.toString());
            }
        });

        this.setState({
            firstName: '',
            lastName: '',
            clientEmail: '',
            location: '',
        });
    };

    render() {
        return(

            <form className='react-form' onSubmit={this.handleSubmit}>

                <img src={logo} className="App-logo" alt="logo" />
                <h1>Create new photo event</h1>

                <fieldset className='form-group'>
                    <ReactFormLabel htmlFor='formName' title='CLIENT NAME' />

                    <input id='formName' className='form-input' name='name' type='text' required onChange={this.handleChange} value={this.state.name} />
                </fieldset>

                <fieldset className='form-group'>
                    <ReactFormLabel htmlFor='formName' title='INDUSTRY' />
                     <p></p>
                        <select>
                            <option value="School">School</option>
                            <option value="Kindergarden">Kindergarden</option>
                            <option value="Sport">Sport</option>
                         </select>
                </fieldset>

                <fieldset className='form-group'>
                    <ReactFormLabel htmlFor='formSubject' title='LOCATION (optional)'/>

                    <input id='formLocation' className='form-input' name='location' type='text' required onChange={this.handleChange} value={this.state.location} />
                </fieldset>

                <fieldset className='form-group'>
                    <ReactFormLabel htmlFor='formEmail' title='EMAIL' />

                    <input id='formEmail' className='form-input' name='clientEmail' type='email' required onChange={this.handleChange} value={this.state.clientEmail} />
                </fieldset>

                <div className='form-group'>
                    <input id='formButton' className='btn' type='submit' placeholder='Send message' />
                </div>
            </form>
        )
    }
}

ReactDOM.render(<ReactForm />, reactFormContainer);
